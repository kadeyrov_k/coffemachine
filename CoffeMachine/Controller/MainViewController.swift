//
//  MainViewController.swift
//  CoffeMachine
//
//  Created by Kadir Kadyrov on 14.07.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var textLableOfCoffeeMachine: UITextField!
    @IBOutlet weak var currentWaterProgress: UIProgressView!
    @IBOutlet weak var currentMilkProgress: UIProgressView!
    @IBOutlet weak var currentBeansProgress: UIProgressView!
    
    let coffeeMachine = CoffeeMachineModel(milk: 0, beans: 0, water: 0, capacityMilk: 1000, capacityWater: 1000, capacityBeans: 1000)

    @IBAction func addWater(_ sender: Any) {
        textLableOfCoffeeMachine.text = coffeeMachine.addWater(add: 500)
        changeProgress(water: coffeeMachine.getWater(), milk: coffeeMachine.getMilk(), beans: coffeeMachine.getBeans())
    }
    @IBAction func addMilk(_ sender: Any) {
        textLableOfCoffeeMachine.text = coffeeMachine.addMilk(add: 500)
        changeProgress(water: coffeeMachine.getWater(), milk: coffeeMachine.getMilk(), beans: coffeeMachine.getBeans())
    }
    @IBAction func addBeans(_ sender: Any) {
        textLableOfCoffeeMachine.text = coffeeMachine.addBeans(add: 100)
        changeProgress(water: coffeeMachine.getWater(), milk: coffeeMachine.getMilk(), beans: coffeeMachine.getBeans())
    }
    
    @IBAction func makeMocco(_ sender: Any) {
        printMessege(coffee: .mocco)
    }
    
    @IBAction func makeCappuccino(_ sender: Any) {
        printMessege(coffee: .capuchino)
    }
    
    @IBAction func makeLatte(_ sender: Any) {
        printMessege(coffee: .latte)
    }
    
    @IBAction func nameBlackCoffee(_ sender: Any) {
        printMessege(coffee: .blackCoffee)
    }
    
    func printMessege (coffee: Coffee) {
        textLableOfCoffeeMachine.text = coffeeMachine.makeCoffee(coffee: coffee)
        changeProgress(water: coffeeMachine.getWater(), milk: coffeeMachine.getMilk(), beans: coffeeMachine.getBeans())
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        changeProgress(water: coffeeMachine.getWater(), milk: coffeeMachine.getMilk(), beans: coffeeMachine.getBeans())
        
        // Do any additional setup after loading the view.
    }
    
    func changeProgress (water: Int, milk: Int, beans: Int) {
        currentMilkProgress.progress = Float(milk) / Float(coffeeMachine.capacityMilk)
        currentWaterProgress.progress = Float(water) / Float(coffeeMachine.capacityWater)
        currentBeansProgress.progress = Float(beans) / Float(coffeeMachine.capacityBeans)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
