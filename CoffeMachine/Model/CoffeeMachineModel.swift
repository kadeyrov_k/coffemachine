//
//  CoffeeMachineModel.swift
//  CoffeMachine
//
//  Created by Kadir Kadyrov on 14.07.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import Foundation


///class for coffee machine, functions make black coffee, latte, capuchino, mocco, add milk, add water, add beans
class CoffeeMachineModel {
    private var milk, beans, water: Int
    let capacityMilk, capacityBeans, capacityWater: Int
    
    init(milk: Int, beans: Int, water: Int, capacityMilk: Int, capacityWater: Int, capacityBeans: Int) {
        self.beans = beans
        self.milk = milk
        self.water = water
        self.capacityBeans = capacityBeans
        self.capacityWater = capacityWater
        self.capacityMilk = capacityWater
    }
    
    func addWater (add: Int) -> String {
        if water + add > capacityWater {
            return "More then allowable capacity"
        } else {
            water += add
            return "Added \(add) of water"
        }
        
    }
    
    func addBeans (add: Int) -> String {
        if beans + add > capacityBeans {
            return "More then allowable capacity"
        } else {
            beans += add
            return "Added \(add) of beans"
        }
        
    }
    
    func addMilk (add: Int) -> String {
        if milk + add > capacityMilk {
            return "More then allowable capacity"
        } else {
            milk += add
            return "Added \(add) of milk"
        }
    }
    
    func getMilk () -> Int {
        return milk
    }
    
    func getWater () -> Int {
        return water
    }
    
    func getBeans () -> Int {
        return beans
    }
    
    
    ///check can we make coffee (beans, milk, water) return error or true
    func makeCoffee (coffee: Coffee) -> String {
        if coffee.beansNeeded > self.beans {
            return "Don`t have beans"
        } else if coffee.milkNeeded > self.milk {
            return "Don`t have milk"
        } else if coffee.waterNeeded > self.water {
            return "Don`t have water"
        } else {
            water -= coffee.waterNeeded
            beans -= coffee.beansNeeded
            milk -= coffee.milkNeeded
            return "Your \(coffee)"
        }
    }
    
}
