//
//  Coffee.swift
//  CoffeMachine
//
//  Created by Kadir Kadyrov on 14.07.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import Foundation

enum Coffee: String {
    case latte = "Latte"
    case blackCoffee = "Black coffee"
    case capuchino = "Cappuccino"
    case mocco = "Mocco"
    
    var beansNeeded: Int {
        switch self {
        case .latte:
            return 10
        case .blackCoffee:
            return 20
        case .capuchino:
            return 10
        case .mocco:
            return 10
        default:
            return 0
        }
    }
    
    var milkNeeded: Int {
        switch self {
        case .latte:
            return 50
        case .blackCoffee:
            return 0
        case .capuchino:
            return 30
        case .mocco:
            return 40
        default:
            return 0
        }
    }
    
    var waterNeeded: Int {
        switch self {
        case .latte:
            return 200
        case .capuchino:
            return 100
        case .blackCoffee:
            return 250
        case .mocco:
            return 150
        default:
            return 0
        }
    }
}


